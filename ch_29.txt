1.
[serena@student ~]$ openssl passwd hunter2
CBXmJlbGN4B7E

[serena@student ~]$ passwd
Changing password for user serena.
Current password: 
New password: 
Retype new password: 
passwd: all authentication tokens updated successfully.

2.
[root@student ~]# tail -1 /etc/shadow
venus:$6$.eIf7EgC/t/sTr73$ypAK5HB6GoFRL7SplizysIMDQX3d3geCFxUtlMWBGHahW0H1VLhaekCUUsdhRz7yJ2CaSYWHm4fvSq/c.Ld9T1:18560:0:99999:7:::

[root@student ~]# usermod -L venus 

[root@student ~]# tail -1 /etc/shadow
venus:!$6$.eIf7EgC/t/sTr73$ypAK5HB6GoFRL7SplizysIMDQX3d3geCFxUtlMWBGHahW0H1VLhaekCUUsdhRz7yJ2CaSYWHm4fvSq/c.Ld9T1:18560:0:99999:7:::

3.

[root@student ~]# tail /etc/shadow | grep ser
serena:$6$ecQec8LKeA4.HDVm$XBWeI6oZeqnD3Lf6FcmbGaW8fjBZYTIPmep2UWse5xOkTlVrqBfidurvutno90SJNN3BHdSjhf/QWNjdSP.Om0:18560:0:99999:7:::

[root@student ~]# passwd -d serena 
Removing password for user serena.
passwd: Success

[root@student ~]# tail /etc/shadow | grep ser
serena::18560:0:99999:7:::

4.
usermod -L "username" הפקודה הזו נועלת את המשתמש ולא ניתן להיכנס למשתמש ,צריך לשחרר את המשתמש דרך רוט להיכנס למשתמש

passwd -d "username" מבטלת את הסיסמאת כניסה.

5.
i can't to chang the password to serena i get ..

[serena@student ~]$ passwd 
Changing password for user serena.
Current password: 
New password: 
BAD PASSWORD: The password is shorter than 8 characters
passwd: Authentication token manipulation error

6.

[root@student ~]# chage -l serena
Last password change					: Oct 25, 2020
Password expires					: never
Password inactive					: never
Account expires						: never
Minimum number of days between password change		: 0
Maximum number of days between password change		: 99999
Number of days of warning before password expires	: 7

[root@student ~]# chage -m 10 serena

[root@student ~]# chage -l serena
Last password change					: Oct 25, 2020
Password expires					: never
Password inactive					: never
Account expires						: never
Minimum number of days between password change		: 10
Maximum number of days between password change		: 99999
Number of days of warning before password expires	: 7

7.
[root@student ~]# grep ^PASS  /etc/login.defs 
PASS_MAX_DAYS	99999
PASS_MIN_DAYS	0
PASS_MIN_LEN	5
PASS_WARN_AGE	7
[root@student ~]# vi /etc/login.defs 
[root@student ~]# grep ^PASS  /etc/login.defs 
PASS_MAX_DAYS	99999
PASS_MIN_DAYS	10
PASS_MIN_LEN	5
PASS_WARN_AGE	7

9.

It is possible to edit the files /etc/passwd and /etc/shodew only in vipw,
not in vi because there is no permission to edit  the files in vi,
whoever I read in the wiki the vipw comes with the "shadow" software package

10.
[root@student ~]# chsh -l
/bin/sh
/bin/bash
/usr/bin/sh
/usr/bin/bash

[root@student ~]# cat /etc/shells 
/bin/sh
/bin/bash
/usr/bin/sh
/usr/bin/bash

11.

[root@student ~]# grep ^serena /etc/shadow
serena:$6$WcJm5ZJh.q/TtxBF$C/5ukHmsAw1JmzTzBt8e3Tsv6ECEFbEjvcdCiz4dy8ioPwlI6hKZ9UaKVtTsMald9qwcOzapVIDEeH0sNEO7g/:18560:10:99999:7:::
[root@student ~]# passwd -l serena
Locking password for user serena.
passwd: Success
[root@student ~]# grep ^serena /etc/shadow
serena:!!$6$WcJm5ZJh.q/TtxBF$C/5ukHmsAw1JmzTzBt8e3Tsv6ECEFbEjvcdCiz4dy8ioPwlI6hKZ9UaKVtTsMald9qwcOzapVIDEeH0sNEO7g/:18560:10:99999:7:::

